CI_REGISTRY_IMAGE ?= gitlab-runner-helper
VERSION ?= v13.9.0
REVISION ?= 2ebc4dc4

LINUX_PLATFORMS += x86_64
#LINUX_PLATFORMS += 386
#LINUX_PLATFORMS += s390x
#LINUX_PLATFORMS += ppc64le
#LINUX_PLATFORMS += arm-v6
#LINUX_PLATFORMS += arm-v7
#LINUX_PLATFORMS += arm64-v8

build-linux:
	@ make $(foreach PLATFORM,$(LINUX_PLATFORMS),platform-linux-$(PLATFORM))

platform-linux-%:
	@ curl -o gitlab-runner-helper https://gitlab-runner-downloads.s3.amazonaws.com/${VERSION}/binaries/gitlab-runner-helper/gitlab-runner-helper.$(subst linux-,,$(subst platform-,,$@))
	@ docker buildx build \
		--platform $(subst -,/,$(subst platform-,,$@)) \
		--build-arg "TAG=buster" \
		-t $(CI_REGISTRY_IMAGE):$(VERSION)-$(subst -,,$(subst linux-,,$(subst platform-,,$@)))-$(REVISION) .

push:
	@ docker push ${CI_REGISTRY_IMAGE}

push-multiarch:
	@ docker manifest create --amend ${CI_REGISTRY_IMAGE}:${VERSION} $(foreach PLATFORM,$(LINUX_PLATFORMS),${CI_REGISTRY_IMAGE}:${VERSION}-$(subst -,,$(PLATFORM))-$(REVISION))
	@ docker manifest push --purge ${CI_REGISTRY_IMAGE}:${VERSION}
