#!/bin/bash

additions=(
    /bin/busybox
    /bin/bash
    /usr/bin/git
    /usr/lib/git-core/*
    /usr/lib/git-core/*/*
    /usr/bin/git-lfs
    /usr/bin/dumb-init

    # busybox needs libnss
    /lib/x86_64-linux-gnu/libnss_*
)

subtractions=(
    # these are the only git commands that still require perl.
    # nothing of which we're likely to use in the helper binary.
    /usr/lib/git-core/git-instaweb
    /usr/lib/git-core/git-add--interactive
    /usr/lib/git-core/git-request-pull
)

for d in "${subtractions[@]}"
do
    rm -f $d
done

mkdir /rootfs
for d in "${additions[@]}"
do
    if [ -f $d ]
    then
        # copy the file
        cp --verbose --parents -P "${d}" /rootfs

        for lib in $(ldd $d | cut -d '>' -f 2 | awk '{print $1}')
        do
            if [ -f "${lib}" ]; then
                # copy the files library dependencies
                cp --verbose --parents -P "${lib}" /rootfs

                # if that library is a symlink, also copy over the symlink
                if [ -L "${lib}" ]; then
                    cp --verbose --parents -P $(readlink -f "${lib}") /rootfs
                fi
            fi
        done
    fi
done
