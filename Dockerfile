ARG TAG

FROM debian:$TAG

RUN apt-get update && apt-get install git dumb-init git-lfs bash busybox ca-certificates -y

# copy files and their dependencies
COPY copy.sh /copy.sh
RUN /copy.sh

# create devices
RUN mkdir rootfs/dev
RUN mknod -m 0666 rootfs/dev/null    c 1 3
RUN mknod -m 0666 rootfs/dev/random  c 1 8
RUN mknod -m 0666 rootfs/dev/urandom c 1 9

# install busybox into rootfs
RUN chroot rootfs /bin/busybox --install /bin

# copy some other needed files
RUN mkdir rootfs/etc rootfs/tmp
RUN cp /etc/nsswitch.conf rootfs/etc/
RUN cp /etc/group rootfs/etc/
RUN cp /etc/passwd rootfs/etc/
RUN cp /etc/shadow rootfs/etc/
RUN cp /etc/hosts rootfs/etc/
RUN cp /etc/hostname rootfs/etc/
RUN cp /etc/resolv.conf rootfs/etc/
RUN ln -vL /usr/share/zoneinfo/UTC rootfs/etc/localtime

# copy git templates
RUN mkdir -p /rootfs/usr/share/git-core/templates && \
    cp -r /usr/share/git-core/templates /rootfs/usr/share/git-core/templates
RUN cp /etc/gitconfig rootfs/etc/
RUN mkdir -p rootfs/root/ && cp -r /root rootfs/

# copy certificates
RUN mkdir -p /rootfs/etc/ssl/certs/ && \
    mkdir -p /rootfs/usr/share/ca-certificates && \
    mkdir -p rootfs/etc/ca-certificates/update.d && \
    mkdir -p rootfs/usr/sbin/ && \
    cp -r /etc/ssl/certs rootfs/etc/ssl/certs && \
    cp /etc/ca-certificates.conf rootfs/etc/ && \
    cp -r /usr/share/ca-certificates rootfs/usr/share/ca-certificates && \
    cp -r /etc/ca-certificates/update.d rootfs/etc/ca-certificates/update.d && \
    cp /usr/sbin/update-ca-certificates rootfs/usr/sbin/

# git-lfs setup
RUN chroot rootfs /usr/bin/git-lfs install --skip-repo

# copy binaries
ADD gitlab-runner-helper /rootfs/usr/bin/gitlab-runner-helper
ADD entrypoint /rootfs/entrypoint
ADD gitlab-runner-build /rootfs/usr/bin/
RUN chmod +x /rootfs/usr/bin/gitlab-runner-helper && \
    chmod +x /rootfs/entrypoint && \
    chmod +x /rootfs/usr/bin/gitlab-runner-build

FROM scratch

COPY --from=0 /rootfs/ ./

ENTRYPOINT ["/entrypoint"]
CMD ["sh"]